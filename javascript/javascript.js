(function($){
	$(document).ready(function(){

		function bloc_depliables(){
			// L'animation des blocs dépliables dans les articles
			$(".deplier-texte").hide();
			$(".deplier-titre").click(function() {
				$(this).toggleClass("deplie");
				$(this).parent().parent().children(".deplier-texte").slideToggle();
				return false;
			});
		}
		bloc_depliables();
		onAjaxLoad(bloc_depliables);
		
		function formulaires_funky(){
			/* Les labels des formulaires funky / animés */
			$('.formulaire_spip input').focus(function() {
				$(this).parent().addClass("focused");
			});
			$('.formulaire_spip input').blur(function() {
				if($(this).val() == '') {
					$(this).parent().removeClass("focused");
				}
			});
			$('.formulaire_spip textarea').focus(function() {
				$(this).parent().addClass("focused");
			});
			$('.formulaire_spip textarea').blur(function() {
				if($(this).val() == '' ) {
					$(this).parent().removeClass("focused");
				}
			});
		}
		formulaires_funky();
		onAjaxLoad(formulaires_funky);
		
	});
})(jQuery);