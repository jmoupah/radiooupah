# Squelette Radio Oupah

Squelette ultra simple "type Tumblr" pour Spip visible par là : https://radiooupah.netwazoo.info
Il n'y a qu'une page (sommaire) qui affiche soit la liste des sites syndiqués, soit un site syndiqué spécifique (si ?i=X) et puis... c'est tout !

# Nécessite

- oembed pour afficher les playlists (Youtube dans mon cas mais doit fonctionner avec tous les services supportés par oembed)
- spip_reset pour ne pas afficher les autres pages

# Utilise

- contact

# Notes

Le site n'utilise pas les articles mais, pour pouvoir ajouter des sites syndiqués, il faut quand même créer une rubrique, même si elle reste vide.

Le flux rss est adapté pour fonctionner avec les sites syndiqués.

# Licence

Le squelette est disponible sous licence CC by-nc-sa https://creativecommons.org/licenses/by-nc-sa/4.0/

Les icônes proviennent de https://orioniconlibrary.com/
